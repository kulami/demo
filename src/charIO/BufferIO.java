package charIO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

public class BufferIO {
	public static void main(String args[]) throws IOException {

		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in, "UTF-8"));
		char[] cbuf = new char[100];
	    bufferedReader.read(cbuf);
		PrintWriter printWriter = new PrintWriter(System.out);
		printWriter.println(cbuf);
		printWriter.close();
	}
}
