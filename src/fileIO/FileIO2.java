package fileIO;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.InputMap;

public class FileIO2 {
	public  static void main(String args[]) throws IOException{
		FileInputStream inputStream = new FileInputStream("F:\\sonia\\Documents\\a.txt");
		FileOutputStream outputStream = new FileOutputStream("F:\\sonia\\Documents\\c.txt");
		int c;
		
		try {
			while((c = inputStream.read()) != -1){
				outputStream.write(c);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if (inputStream != null) {
				inputStream.close();
			}
			if (outputStream != null) {
				outputStream.close();
			}
		}
		
	}
}
