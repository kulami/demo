package scanner;

import java.io.InputStream;
import java.util.Scanner;

import javax.swing.plaf.synth.SynthSeparatorUI;

public class Scanning {
	
	public static void main(String args[]){
		Scanner scanner = new Scanner(System.in);
		
		while(scanner.hasNext()){
			System.out.println(scanner.next());
		}
		scanner.close();
		
	}


}
